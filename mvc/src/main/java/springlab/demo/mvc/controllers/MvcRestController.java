package springlab.demo.mvc.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springlab.demo.mvc.entities.Greeting;

@RestController
public class MvcRestController {

    @GetMapping("/rest")
    public Greeting greet(@RequestParam(defaultValue = "Hello World!") String name){
        return new Greeting("Hello " + name );

    }
}

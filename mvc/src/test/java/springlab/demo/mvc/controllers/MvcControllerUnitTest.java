package springlab.demo.mvc.controllers;

import org.junit.Test;
import org.springframework.validation.support.BindingAwareModelMap;

import static org.junit.Assert.*;

public class MvcControllerUnitTest {

    @Test
    public void sayHello() {
        MvcController controller = new MvcController();
        BindingAwareModelMap model = new BindingAwareModelMap();
        //String result = controller.sayHello("Dmitriy", new BindingAwareModelMap());
        String result = controller.sayHello("Dmitriy", model);
        System.out.println(model.asMap().get("user"));
        assertEquals("hello", result);
        assertEquals("Dmitriy", model.asMap().get("user"));
    }
}